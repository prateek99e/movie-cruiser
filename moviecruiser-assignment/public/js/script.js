// let movies=[];
// let list ="";
// var xhr = new XMLHttpRequest();
// function getMovies() {
// 	xhr.open("get", "http://localhost:3000/movies");
// 	xhr.onload= function ()
// 	{
// 		console.log(JSON.parse(xhr.responseText));
// 		movies = JSON.parse(xhr.responseText);
// 		alert(movies.length)
// 		movies.forEach(element => {
			 
// 			list+=`<div id="${element.id}">
// 			<h6> ${element.title} </h6> 
// 			<img src="${element.posterPath}">
// 			<button type="button" onClick="Add(${element)">.id Add to Fav </button> </div>`
// 		});
// 		console.log("data")
// 		document.getElementById("moviesList").innerHTML=list;
// 	}
	
// 	xhr.send(null);


// }

// function getFavourites() {

// }

// function addFavourite(id) {

// }

// getMovies();
// // module.exports = {
// // 	getMovies,
// // 	getFavourites,
// // 	addFavourite
// // };

// // You will get error - Uncaught ReferenceError: module is not defined
// // while running this script on browser which you shall ignore
// // as this is required for testing purposes and shall not hinder
// // it's normal execution
  

let movieItems;
let favItems=[];
 
const getMovies = () => {
	return fetch("http://localhost:3000/movies")
	.then((result) => {
		if (result.status == 200) {
			return Promise.resolve(result.json());
		} else {
			return Promise.reject("Unable to retrieve the movie list");
		}
	}).then(resultMovie => {
		movieItems = resultMovie;
		//Populate into the DOM
		createMovieList();
		return movieItems;
	}).catch(error => {
		throw new Error(error);
	})
}

//Post Movie API
const postMovie = (myMovie) => {
	return fetch("http://localhost:3000/favourites", {
		method: 'POST',
		body: JSON.stringify(myMovie),
		headers: {
			'Content-Type': 'application/json',
			'Accept': 'application/json'
		}
	}).then((result) => {
		if (result.status == 201) {
			return Promise.resolve();
		} else {
			return Promise.reject("Movie is already added to favourites");
		}
	})
}

//Get the Favourites Movie list
function getFavourites() {
	//API call
	return fetch("http://localhost:3000/favourites").then((result) => {
		if (result.status == 200) {
			return Promise.resolve(result.json());
		} else {
			return Promise.reject("Error");
		}
	}).then(result => {
		favItems = result;
		createFavouriteList();
		return result;
	}).catch(error => {
		throw new Error(error);
	})

}

function addFavourite(id) {
	console.log("oo")
	console.log(id)
	if (!isMoviePresentInFavItems(id)) {
		let movieObject = getMovieById(id)
		favItems.push(movieObject);
		console.log(favItems)
		//Add Favourite call
		return fetch("http://localhost:3000/favourites", {
			method: 'POST',
			body: JSON.stringify(movieObject),
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json'
			}
		}).then((result) => {
			if (result.status == 200 || result.status == 201) {
				return Promise.resolve(favItems);
			} else {
				
			
				return Promise.reject("Movie is already added to favourites");
			}
		}).then((favMovieResult) => {
			createFavouriteList();
			return favMovieResult;
		}).catch(err => {
			alert("Movie is already added to favourites");
			// throw new Error(err);
		})

	} else {
		alert("Movie is already added to favourites");
		// throw new Error("Movie is already added to favourites");
	}

}

function isMoviePresentInFavItems(selectedMovieId) {
	for (let favmovie in favItems) {
		if (selectedMovieId == favItems[favmovie].id) {
			return true;
		}
	}
	return false;
}

function getMovieById(id) {
	console.log(id);
	for (let movie in movieItems) {
		if (id == movieItems[movie].id) {
			console.log(movieItems[movie])
			return movieItems[movie];
		}
	}
}

const createMovieList = () => {
	let domMovieList = '';
	movieItems.forEach(element => {
		// domMovieList = domMovieList + `
		// <div id="${element.id}" class="list-group-item d-flex flex-column align-items-center">
		// <h6>${element.title}</h6>
		// <img src="${element.posterPath}" class="img-fluid pb-2" alt="Responsive image">

		// <p><strong>Description</strong>: <span id="Description">${element.Descriptions}</span></p>

		// <p>Year: <span id="year">${element.releaseDate}</span></p>

		// <button
		// onclick="addFavourite(${element.id})" type="button" class="btn btn-primary">
		// Add to Favourites
		// </button><br>

	

		// </div>
		
		// `;
		domMovieList = domMovieList + `<div class="card" style="width:400px">
		<img class="img-fluid " src="${element.posterPath}" alt="Card image">
		<div class="card-body">
		  <h4 class="card-title">${element.title}</h4>
		  <p class="Description">${element.Descriptions}</p>
		  <button
		  onclick="addFavourite(${element.id})" type="button" class="btn btn-primary">
		 Add to Favourites
		  </button><br>		</div>
	  </div><br>`;
	});
	document.getElementById("moviesList").innerHTML = domMovieList;
}

const createFavouriteList = () => {
	let domFavouriteList = '';
	let childNode = document.getElementById("favouritesList");
	childNode.innerHTML = '';
	favItems.forEach(element => {
		 domFavouriteList = domFavouriteList + `<div class="card" style="width:400px">
		 <img class="img-fluid " src="${element.posterPath}" alt="Card image">
		 <div class="card-body">
		   <h4 class="card-title">${element.title}</h4>
		   <p class="Description">${element.Descriptions}</p>
		   <button
		   onclick="deletefav(${element.id})" type="button" class="btn btn-danger">
		    Remove from Favourite
		    </button><br>		</div>
	   </div><br>`;
		//`
		// <div id="${element.id}" class="list-group-item d-flex flex-column align-items-center">
		// <h6>${element.title}</h6>
		// <img src="${element.posterPath}" class="img-fluid pb-2" alt="Responsive image">

		// <p><strong>Description</strong>: <span id="Description">${element.Descriptions}</span></p>

		// <p>Year: <span id="year">${element.releaseDate}</span></p>

		// <button
		// onclick="deletefav(${element.id})" type="button" class="btn btn-danger">
		// Remove
		// </button>
		// </div>
		// `;
	});
	childNode.innerHTML = domFavouriteList;
}

function deletefav(id){
	favItems=favItems.filter(item=>item.id !== id);
	fetch('http://localhost:3000/favourites/' + id,{
		method:'DELETE',
	})
	.then((result)=>{
	if(result.status==200){
		createFavouriteList();
	}else{
		alert("Deleted")
		// throw new Error("unable to remove")

	}
})
.catch(error=>{
	alert("Deleted")
	// throw new Error(error);
});
}
 getMovies();
getFavourites();